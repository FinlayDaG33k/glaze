# DEVELOPMENT MOVED!
Development on Glaze has moved [here](https://gitlab.com/admiralcms/port)
# Glaze
CakePHP development stack containing Nginx, PHP, Composer and Sass.  
All that's needed is for your machine to support Bash (Comes out of the box with most GNU+Linux distros and is easily installable on Windows 10).

It allows you to get started with developing in little to no time while also providing you with access to Cake and Composer's commands!

# Quick Start
A Quick Start guide is available in the documentation [here](docs/quick-start.md).

# Installing
A guide to install Glaze is available in the [Quick Start](docs/quick-start.md).

# Usage
A list of all commands and some examples is available in the [Commands Reference](docs/commands.md).