# Introduction to Glaze
Glaze is a docker based Development stack for CakePHP that allows you to keep your local dependencies to a minimum.  
By default, it has an image for everything a CakePHP should need like:
- A Webserver (NginX)
- PHP (running php-fpm 7.3.6)
- A Database server (MySQL)
- Sass (running NodeJS 10 and Gulp 3)
- Composer

The only thing you need locally is Docker and Docker Compose.

# Why Glaze?
I personally used a lot of Docker in my workflow, however, I still had to deal with stuff like Composer, Php etc. etc.  
Now only I had to do this but my colleagues as well, due to our System Policies, it was a pain to install and update these dependencies, which inhibited our workflow.  

Not only that but it was often a mix-and-match of different projects, their dependencies and their project folders since they (like most new developers) worked with tools like Wampp and Xampp.  
This meant that they often ran outdated versions of certain things (like PHP), things outright didn't work (because devA used Apache2 and devB used Nginx) or because of missing extensions (a common one was the ext-intl for PHP).
This often meant that I was called in to "diagnose" the issue (wasting my time), which often led to me having to call in the system administrator (wasting his time) just to fix this.

Because of this, I started to look around and found that Laravel has something called "[Homestead](https://github.com/laravel/homestead)".  
One standardized stack with all the dependencies setup.

But there where two problems with it:
- We don't use Laravel
- My colleague's workstations are to weak to handle Vagrant (seriously, the bloody things only run on a dual-core i5 with 4GB of RAM and a slow HDD... have fun with that)
- I already used a lot of Docker
- I already disliked Vagrant

So, I set out to build something like Homestead but for Cake, based on Docker.
So I [built](https://gitlab.com/FinlayDaG33k/Website/commit/61173d773fff8eb8f6bfff5c88b5d4c9ca67875f) Glaze.  
First, it started out as a small bat script (with a Bash version alongside it), as well as some Docker images for the CMS we've been working on.
Then later as a stand-alone project (which you are currently looking at).  

While not without it's issues, Glaze had helped a lot with the workflow and my colleagues could now focus more on writing the code instead of dealing with the dependencies and running over to our system administrators (or more importantly, me) to install a dependency.