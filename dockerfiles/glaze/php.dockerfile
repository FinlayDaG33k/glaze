FROM php:fpm

# Set some fancy labeling
LABEL maintainer="me@finlaydag33k.nl"

# Update the apt
RUN apt-get update

# Add intl
RUN apt-get install -y \
libicu-dev
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl

# Add PDO
RUN docker-php-ext-install pdo pdo_mysql

# Add GD
RUN apt-get install -y \
libpng-dev libwebp-dev libjpeg-dev libz-dev libxpm-dev
RUN docker-php-ext-configure gd \
    --with-jpeg-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-xpm-dir=/usr/include \
    --with-webp-dir=/usr/include/
RUN docker-php-ext-install gd

# Add APCu
RUN pecl install apcu && docker-php-ext-enable apcu

# Add OPcache
RUN docker-php-ext-install opcache