FROM composer:1.6.5

CMD ["composer", "install", "--ignore-platform-reqs", "--no-scripts"]
