#!/usr/bin/env sh

# wait until MySQL is really available
maxcounter=300
 
counter=1
while ! curl -s mysql:3306 > /dev/null 2>&1; do
    sleep 1
    counter=`expr $counter + 1`
    if [ $counter -gt $maxcounter ]; then
        >&2 echo "We have been waiting for MySQL too long already; failing."
        exit 1
    fi;
done

>&2 echo "MySQL became active!"

bin/cake install migrations

echo "Changing permissions on logs"
chown -R www-data:root /var/www/logs

echo "Changing permissions on tmp"
chown -R www-data:root /var/www/tmp

echo "Chaning permissions on uploads"
chown -R www-data:root /var/www/webroot/uploads

php-fpm
